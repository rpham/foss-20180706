/**
 * Divide numbers
 * @return {Number}   The result
 */
function div (a, b) {
  // TODO
  return 0
}

/**
 * Substract numbers
 * @return {Number}   The result
 */
function sub (a, b) {
  // TODO
  return 0
}

/**
 * Add numbers
 * @return {Number}   The result
 */
function add (a, b) {
  // TODO
  return 0
}

/**
 * Multiply numbers
 * @return {Number}   The result
 */
function mult (a, b) {
  // TODO
  return 0
}

/**
 * The conway sequence looks like: 1 11 21 1211 111221 ...
 * @param  {String} s Initial value
 * @return {String}   Next value or null if the initial value is invalid
 */
function conwayNext (s) {
  // TODO
  return ''
}

module.exports = {
  mult,
  add,
  div,
  sub,
  conwayNext
}
